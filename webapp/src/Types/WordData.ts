type WordData = {
    _id: string,
    word: string,
    wordTranslation: string,
    wordReading: string,
    sentence: string,
    sentenceTranslation: string,
    sentenceReading: string
};

export type FrontKey = "word" | "wordReading" | "wordTranslation" | "sentence" | "sentenceReading" | "sentenceTranslation";

export default WordData;