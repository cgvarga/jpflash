import WordData from './WordData';

type ListData = {
    _id: string,
    name: string,
    words: WordData[]
};

export default ListData;