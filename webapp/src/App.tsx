import React from 'react';
import './App.css';

import { useSelector } from "react-redux";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Home from "./routes/home/home";
import Quiz from "./routes/quiz/quiz";
import Lists from "./routes/lists/lists";
import Words from "./routes/words/words";
import Results from "./routes/results/results";
import Login from './routes/login/login';

import {
  authorized
} from './slices/authSlice';
import TheHeader from './components/TheHeader';
import { Container } from '@mui/material';
import { Box } from '@mui/system';

function App() {
  const isAuthorized = useSelector(authorized);
  const base = window.location.pathname;

  return (
    <Container
      component="main"
      className="App"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: 0
      }}
      >
      <Router basename={base}>
        {isAuthorized &&
          <TheHeader />
        }
        <Box
          className="main-content flex-row content">
          <Switch>
            <Route path="/quiz">
              <Quiz />
            </Route>
            <Route path="/lists">
              <Lists />
            </Route>
            <Route path="/words">
              <Words />
            </Route>
            <Route path="/results">
              <Results />
            </Route>
            <Route path="/home">
              <Home />
            </Route>
            <Route path="/">
              <Login />
            </Route>
          </Switch>
          {!isAuthorized &&
            <Redirect to="/" />
          }
        </Box>
      </Router>
    </Container>
  );
}

export default App;
