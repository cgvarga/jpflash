import { createSlice } from "@reduxjs/toolkit";
import serviceManager, {getUrl} from "../modules/serviceManager";

export const authSlice = createSlice({
    name: "auth",
    initialState: {
        user: null,
        authorized: false,
        hasError: false,
        authError: null,
        isRegistering: false
    },
    reducers: {
        login: (state, action) => {
            state.user = action.payload;
            state.authorized = true;
            state.hasError = false;
            state.authError = null;
        },
        userError: (state, action) => {
            state.hasError = true;
            state.authError = action.payload.error;
        },
        register: (state, action) => {
            state.isRegistering = action.payload;
        },
        registered: (state, action) => {
            state.isRegistering = false;
        },
        logout: (state, action) => {
            state.authorized = false;
        }
    }
});

export const { login, register, registered, userError, logout} = authSlice.actions;

export const loginHandshake = ({username, password}) => dispatch => {

    serviceManager.exec({
        url: getUrl('auth.login'),
        method: 'POST',
        body: {username, password}
    })
        .then((resp) => {
            dispatch(login({resp}));
        })
        .catch(error => {
            dispatch(userError({type: 'login', error}));
        });
}
export const registerNewUser = ({username, password, email}) => dispatch => {

    serviceManager.exec({
        url: getUrl('auth.register'),
        method: 'POST',
        body: { username, password, email }
    })
        .then((resp) => {
            dispatch(registered());
        })
        .catch(error => {
            dispatch(userError({ type: 'register', error }));
        });
}

export const user = ({auth}) => auth.user;
export const authorized = ({auth}) => auth.authorized;
export const authError = ({auth}) => auth.authError;
export const hasError = ({ auth }) => auth.hasError;
export const isRegistering = ({ auth }) => auth.isRegistering;

export default authSlice.reducer;