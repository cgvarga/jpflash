import { createSlice } from "@reduxjs/toolkit";
import serviceManager, { getUrl } from "../modules/serviceManager";

export const slice = createSlice({
    name: 'words',
    initialState: {
        words: [],
        error: null
    },
    reducers: {
        wordsError: (state, action) => {
            state.error = action.payload;
        },
        gotWords: (state, action) => {
            state.words = action.payload;
            state.error = null;
        }
    }
});

export const getWords = () => (dispatch) => {
    serviceManager.exec({
        url: getUrl('words.all')
    })
    .then((words) => {
        dispatch(gotWords(words));
    })
    .catch((err) => {
        dispatch(wordsError(err));
    })
};

export const {gotWords, wordsError} = slice.actions;

export const words = ({words}) => words.words;

export default slice.reducer;