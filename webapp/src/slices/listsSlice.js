import { createSlice } from "@reduxjs/toolkit";
import serviceManager, { getUrl } from "../modules/serviceManager";

const slice = createSlice({
    name: 'lists',
    initialState: {
        lists: [],
        adding: false,
        deleting: false,
        editing: false
    },
    reducers: {
        gotLists: (state, action) => {
            console.log('got lists');
            state.lists = action.payload;
        },
        addingList: (state, action) => {
            state.adding = action.payload;
        },
        deletingList: (state, action) => {
            state.deleting = action.payload;
        },
        editingList: (state, action) => {
            state.editing = action.payload;
        }
    }
});

export const getLists = () => (dispatch) => {
    serviceManager.exec({
        url: getUrl('lists.mine')
    })
        .then(resp => {
            dispatch(gotLists(resp));
        })
        .catch((err) => {
            console.log(err);
        });
};
export const addList = ({name, words = []}) => (dispatch) => {
    serviceManager.exec({
        url: getUrl('lists.add'),
        method: 'POST',
        body: {name, words}
    })
        .then(() => {
            dispatch(addingList(false));
            dispatch(getLists());
        })
        .catch((err) => {
            console.log(err);
        });
};
export const editList = ({_id, name, words = []}) => (dispatch) => {
    serviceManager.exec({
        url: getUrl('lists.edit', {id: _id}),
        method: 'PUT',
        body: {name, words}
    })
        .then(() => {
            dispatch(editingList(false));
            dispatch(getLists());
        })
        .catch((err) => {
            console.log(err);
        });
};
export const deleteList = (id) => (dispatch) => {
    serviceManager.exec({
        url: getUrl('lists.delete', {id}),
        method: 'delete'
    })
        .then(() => {
            dispatch(deletingList(false));
            dispatch(getLists());
        })
        .catch((err) => {
            console.log(err);
        });
};

export const {gotLists, addingList, deletingList, editingList} = slice.actions;

export const lists = ({lists}) => lists.lists;
export const adding = ({lists}) => lists.adding;
export const editing = ({lists}) => lists.editing;
export const deleting = ({lists}) => lists.deleting;

export default slice.reducer;