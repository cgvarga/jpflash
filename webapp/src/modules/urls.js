const SERVICES_URL = 'https://jpflash-services.herokuapp.com';
// const SERVICES_URL = 'http://localhost:5000';
const urls = {
    auth: {
        login: SERVICES_URL + '/user/login',
        register: SERVICES_URL + '/user/register',
        logout: SERVICES_URL + '/user/logout',
    },
    words: {
        all: SERVICES_URL + '/words'
    },
    lists: {
        mine: SERVICES_URL + '/lists',
        add: SERVICES_URL + '/lists',
        delete: SERVICES_URL + '/lists/#id#',
        edit: SERVICES_URL + '/lists/#id#',
    }
};

export default urls;

