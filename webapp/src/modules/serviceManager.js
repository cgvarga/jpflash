import urls from "./urls";

const defaultOptions = {
    url: '',
    method: 'get',
    mode: 'cors',
    body: null,
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json, text/javascript, */*; q=0.01"
    },
    credentials: 'include'
};

export const getUrl = (path = '', params) => {
    let url = '';
    let api = urls;
    path.split(/\./g).forEach((key) => {
        if (!api) {
            return;
        }
        if (typeof api[key] === 'string') {
            url = api[key];
            return;
        }
        api = api[key];
    });
    if (params) {
        Object.keys(params).forEach(key => {
            const regex = new RegExp(`#${key}#`, 'g');
            url = url.replace(regex, params[key]);
        });
    }
    return url;
}


const serviceManager = {
    exec(options) {
        options = Object.assign({}, defaultOptions, options);
        let isOk = true;
        return new Promise((res, rej) => {
            fetch(
                options.url,
                {
                    method: options.method,
                    mode: 'cors',
                    cache: 'no-cache',
                    headers: options.headers,
                    body: options.body && JSON.stringify(options.body),
                    credentials: options.credentials
                }
            )
            .then((resp) => {
                isOk = resp.ok;
                const contentType = resp.headers.get("content-type");
                if (contentType && contentType.includes('application/json')) {
                    return resp.json()
                        .then((json) => {
                            if (isOk) {
                                res(json);
                                return;
                            }
                            throw json;
                        });
                }
                return resp.text().then((text) => {
                    if (isOk) {
                        res(text);
                        return;
                    }
                    throw text;
                })
            })
            .catch(error => {
                rej(error);
            });
        })
    }
};

export default serviceManager;