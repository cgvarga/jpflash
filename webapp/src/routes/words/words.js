import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/grid/grid";
import { words, getWords } from "../../slices/wordsSlice";

const columns = [
    {
        header: "Word",
        key: "word",
        formatter: null
    },
    {
        header: "Reading",
        key: "wordReading",
        formatter: null
    },
    {
        header: "Translation",
        key: "wordTranslation",
        formatter: null
    },
    {
        header: "Sentence",
        key: "sentence",
        formatter: null
    },
    {
        header: "Reading",
        key: "sentenceReading",
        formatter: null
    },
    {
        header: "Translation",
        key: "sentenceTranslation",
        formatter: null
    },
]

function Words() {

    const data = useSelector(words);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getWords());
    }, [dispatch]);

    return (
        <div className="route-content">
            <h2 className="route-header">Words</h2>
            <div className="route-body">
                <div className="route-view">
                    {data.length > 0 &&
                        <Grid
                            columns={columns}
                            data={data}
                            useId={'_id'}
                            rowSelector={true}/>
                    }
                </div>
            </div>
        </div>
    );
};

export default Words;