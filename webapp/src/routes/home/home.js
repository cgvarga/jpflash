import { List, ListItem } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import {
  Link
} from "react-router-dom";


function Home() {
  return (
    <Box className="route-content">
      <h2 className="route-header">Home</h2>
      <Box className="route-body">
        <nav>
          <List>
            <ListItem><Link to="/quiz">Quiz</Link></ListItem>
            <ListItem><Link to="/lists">Lists</Link></ListItem>
            <ListItem><Link to="/words">Words</Link></ListItem>
            <ListItem><Link to="/results">Results</Link></ListItem>
          </List>
        </nav>
      </Box>
    </Box>
  );
};

export default Home;