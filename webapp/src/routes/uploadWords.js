import React from 'react';

class UploadWords extends React.Component {

    state = {
        hasFile: false,
        data: null
    }

    async _handleUploadChange(evt) {
        const data = await evt.target.files[0].text();
        this.setState({
            hasFile: true,
            data: JSON.parse(data)
        });
    }

    _handleUploadClick() {
        const words = this.state.data;
        const promises = [];
        Object.keys(words).forEach((key, i) => {
            const word = words[key];
            const body = JSON.stringify({
                "word": word.word,
                "wordReading": word.reading,
                "wordTranslation": word.translation,
                "sentence": word.example,
                "sentenceReading": word.example_reading,
                "sentenceTranslation": word.example_trans,
                "rank": word.rank || (i + 1)
            });
            promises.push(
                fetch('http://localhost:5000/words', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body
                })
            );
        });

        Promise.all(promises)
            .then((resp) => {
                console.log(resp);
            });
    }

    render() {

        return (
        <form onSubmit={(evt) => { evt.preventDefault() }}>
            <fieldset>
                <legend>Bulk</legend>
                <label>
                    <input ref="file-input" type="file" accept=".json" onChange={this._handleUploadChange.bind(this)} />
                    <button onClick={this._handleUploadClick.bind(this)} disabled={!this.state.hasFile}>Upload File</button>
                </label>
            </fieldset>
            <fieldset>
                <legend>Single Word</legend>
                <div>
                    <label>Word <input type="text" name="word" id="word" /></label>
                </div>
                <div>
                    <label>Reading <input type="text" name="wordReading" id="wordReading" /></label>
                </div>
                <div>
                    <label>Translation <input type="text" name="translation" id="translation" /></label>
                </div>
                <div>
                    <label>Sentence <textarea name="sentence" id="sentence"></textarea></label>
                </div>
                <div>
                    <label>Sentence Reading <textarea name="sentenceReading" id="sentenceReading"></textarea></label>
                </div>
                <div>
                    <label>Sentence Translation <textarea name="sentenceTranslation" id="sentenceTranslation"></textarea></label>
                </div>
            </fieldset>
        </form>);
    }
};

export default UploadWords;