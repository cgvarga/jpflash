import React, { useState, useEffect } from "react";
import { lists, getLists } from "../../slices/listsSlice";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/grid/grid";
import { Switch, Route } from "react-router";
import QuizViewer from "./quizViewer";
import { useHistory, useRouteMatch } from "react-router-dom";
import Overlay from "../../components/overlay/overlay";
import { fields as cardFields } from "../../components/card/card";
import { InputLabel, MenuItem, Select } from "@mui/material";

const columns = [
    {
        header: 'Name',
        key: 'name'
    },
    {
        header: 'Total Words',
        key: 'totalWords',
        formatter: (rowData) => {
            return String(rowData.words.length);
        },
        width: 50
    },
    {
        header: 'Result',
        key: 'results',
        formatter: (rowData) => {
            return '';
        },
        width: 50
    },
];

function Quiz() {

    const [selectedRow, setSelectedRow] = useState(null);
    const [questionCount, setQuestionCount] = useState(10);
    const [frontKey, setFrontKey] = useState('word');
    const [starting, setStarting] = useState(false);
    const dispatch = useDispatch();
    const data = useSelector(lists);
    const history = useHistory();

    useEffect(() => {
        dispatch(getLists());
    }, [dispatch]);

    const startingQuiz = (rowData) => {
        setSelectedRow(rowData);
        setStarting(true);
    };
    const startQuiz = () => {
        setStarting(false);
        history.push(`${match.url}/viewer`);
    };

    const match = useRouteMatch();

    return (
        <div className="route-content">
            <h2 className="route-header">Quiz</h2>
            <div className="route-body">
                <div className="route-controls">
                    <div className="region"></div>
                    <div className="region"></div>
                </div>
                <div className="route-view">
                    <Switch>
                        <Route path={match.url + "/viewer"}>
                            <QuizViewer
                                list={selectedRow}
                                questionCount={questionCount}
                                frontKey={frontKey} />
                        </Route>
                        <Route path="/">
                            <Grid
                                columns={columns}
                                data={data}
                                useId={'_id'}
                                onRowClick={startingQuiz} />
                            {starting &&
                                <Overlay
                                    title="Quiz Settings"
                                    onClose={() => { setStarting(false) }}
                                    buttons={{ right: [{ label: 'Start', classNames: "start", onClick: startQuiz }] }}>
                                    <form>
                                        <InputLabel id="quiz-settings-front-label">Front</InputLabel>
                                        <Select
                                            name="front"
                                            margin="normal"
                                            labelId="quiz-settings-front-label"
                                            id="quiz-settings-front"
                                            fullWidth
                                            defaultValue={frontKey}
                                            onChange={(evt) => { setFrontKey(evt.target.value) }}>{
                                                cardFields.map(field => {
                                                    return <MenuItem
                                                        key={field.key}
                                                        value={field.key}>{field.header}</MenuItem>
                                                })
                                            }</Select>

                                        <InputLabel id="quiz-settings-question-count-label">Total Questions</InputLabel>
                                        <Select
                                            margin="normal"
                                            labelId="quiz-settings-question-count-label"
                                            id="quiz-settings-question-count"
                                            defaultValue={questionCount}
                                            fullWidth
                                            onChange={(evt) => { setQuestionCount(evt.target.value) }}>{
                                                [5, 10, 15, 20, 25, 50, 'All'].map(count => {
                                                    return <MenuItem key={`total-${count}`} value={count}>{count}</MenuItem>
                                                })
                                            }</Select>
                                    </form>
                                </Overlay>
                            }
                        </Route>
                    </Switch>
                </div>
            </div>
        </div>
    );
};

export default Quiz;