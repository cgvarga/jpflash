import { useState } from "react";
import { useHistory } from "react-router-dom";
import Card from "../../components/card/card";

export default function QuizViewer(props = { list: { words: [] }, questionCount: 10, frontKey:"word" }) {

    const getQuestions = () => {
        const qs = {};
        const available = props.list.words.slice();
        const qCount = props.questionCount === 'All' ? available.length : parseInt(props.questionCount, 10);
        const maxQuestions = available.length < qCount ? available.length : qCount;
        while (Object.keys(qs).length < maxQuestions) {
            const randIndex = Math.floor(Math.random() * (available.length - 1));
            const tarWord = available.splice(randIndex, 1)[0];
            if (tarWord && !qs[tarWord._id]) {
                qs[tarWord._id] = tarWord;
            }
        }
        return Object.keys(qs).map(key => qs[key]);
    };

    const [curQuestion, setCurQuestion] = useState(0);
    const [questions] = useState(getQuestions());
    const [curWord, setCurWord] = useState(questions[0]);
    const [flipped, setFlipped] = useState(false);
    const [correct, setCorrect] = useState({});
    const [wrong, setWrong] = useState({});
    const history = useHistory();

    const handleResult = (isCorrect) => {
        if (isCorrect) {
            const newCorrectAns = {};
            newCorrectAns[curWord._id] = curWord;
            setCorrect(Object.assign({}, correct, newCorrectAns));
        } else {
            const newWrongAns = {};
            newWrongAns[curWord._id] = curWord;
            setWrong(Object.assign({}, wrong, newWrongAns));
        }
        setFlipped(false);
        setCurQuestion(curQuestion + 1);
        setTimeout(() => {
            setCurWord(questions[curQuestion + 1]);
        }, (375));// half of flip animation length
    };


    const handleQuizeComplete = () => {
        history.go(-1);
    };

    return (
        <div className="quiz-viewer flex-col flex-main">
            <div className="quiz-viewer-header header flex-row">{props.list.name}</div>
            <div className="quiz-viewer-content content flex-col flex-main">
                {( curWord && questions.length && curQuestion < questions.length - 1) &&
                <div className="flex-col flex-main">
                    <ul className="progress-bar flex-row flex-space-between flex-wrap">
                        {questions.map((q, i) => {

                            const classes = ['progress-bar-step'];
                            if (i === curQuestion) {
                                classes.push('current');
                            }
                            if (correct[q._id]) {
                                classes.push('correct');
                            }
                            if (wrong[q._id]) {
                                classes.push('wrong');
                            }

                            return (<li
                                    key={`step-${q._id}`}
                                    className={classes.join(' ')}
                                    >{i + 1}</li>);
                        })}
                    </ul>
                    <Card
                        data={curWord}
                        onFlipped={() => {
                            setFlipped(true);
                        }}
                        flipped={flipped}
                        flipOnce={true}
                        frontKey={props.frontKey}
                        classNames='flex-center-self'/>
                </div>
                }
                {curQuestion === questions.length -1 &&
                <div className="flex-col flex-main flex-center">
                    <div className="quiz-result flex-row">
                        Got {Object.keys(correct).length} of {questions.length} Correct!
                    </div>
                    <div className="flex-row flex-main flex-wrap flex-center flex-space-evenly">
                        {questions.map((q, i )=> {
                            return (
                            <div className="flex-col">
                                {i + 1 } {correct[q._id] ? "Correct" : "Wrong"}
                                <Card
                                    data={q}
                                    frontKey="word"
                                />
                            </div>
                            );
                        })}

                    </div>
                    <button onClick={handleQuizeComplete}>Complete</button>
                </div>
                }
            </div>
            <div className="quiz-viewer-footer footer flex-col flex-center">
                {flipped &&
                    <div className="flex-row">
                        <button onClick={() => handleResult(false)}>Wrong</button>
                        <button onClick={() => handleResult(true)}>Correct</button>
                    </div>
                }
                <div className="flex-row">
                    <button onClick={handleQuizeComplete}>Cancel</button>
                </div>
            </div>
        </div>
    );
};