import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    Redirect
} from "react-router-dom";

import {
    loginHandshake,
    authorized,
    register,
    isRegistering,
    registerNewUser,
    hasError,
    authError
} from "../../slices/authSlice";

import { Button, Box, Grid, Link, TextField, Typography } from "@mui/material";

export default function Login() {

    const dispatch = useDispatch();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const registering = useSelector(isRegistering);

    const handleFormChange = (field, evt) => {
        const fieldMap = {
            'username': setUsername,
            'password': setPassword,
            'email': setEmail
        }
        if (fieldMap[field]) {
            fieldMap[field](evt.target.value);
        }
    }

    const handleLogin = () => {
        dispatch(loginHandshake({ username, password }));
    }

    const handleNewUser = () => {
        dispatch(register(!registering));
    }

    const handleRegister = () => {
        dispatch(registerNewUser({ username, password, email }));
    }

    let errorMessage = useSelector(authError);

    return (
        <Box
            className="route-content"
            maxWidth="xs"
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}>
            <Typography
                component="h1"
                className="route-header"
                variant="h5">Login</Typography>
            <Box className="route-body">
                {!useSelector(isRegistering) &&
                    <Box
                        component="form"
                        onSubmit={(evt) => { evt.preventDefault() }}
                        sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            label="Username"
                            type="text" name="username"
                            required
                            fullWidth
                            onChange={handleFormChange.bind(this, "username")}
                            defaultValue={username} />
                        <TextField
                            margin="normal"
                            label="Password"
                            required
                            fullWidth
                            type="password" name="password"
                            defaultValue={password}
                            onChange={handleFormChange.bind(this, "password")} />
                        <Button
                            type="submit"
                            sx={{ mt: 3, mb: 2 }}
                            variant="contained"
                            fullWidth
                            onClick={handleLogin.bind(this)}>Login</Button>
                        <Grid container>
                            <Grid item xs>
                                <Link
                                    variant="body2"
                                    onClick={(evt) => evt.preventDefault()}>Forgot your password?</Link>
                            </Grid>
                            <Grid item xs>
                                <Link
                                    variant="body2"
                                    onClick={handleNewUser.bind(this)}>Dont' have an account? Register!</Link>
                            </Grid>
                        </Grid>
                    </Box>
                }
                {useSelector(isRegistering) &&
                    <Box component="form" onSubmit={(evt) => { evt.preventDefault() }}>
                        <TextField
                            margin="normal"
                            label="Username"
                            type="text" name="username"
                            required
                            fullWidth
                            onChange={handleFormChange.bind(this, "username")}
                            defaultValue={username} />
                        <TextField
                            margin="normal"
                            label="Password"
                            required
                            fullWidth
                            type="password" name="password"
                            defaultValue={password}
                            onChange={handleFormChange.bind(this, "password")} />
                        <TextField
                            margin="normal"
                            label="Email"
                            required
                            fullWidth
                            type="email" name="email"
                            defaultValue={email}
                            onChange={handleFormChange.bind(this, "email")} />
                        <Button
                            type="submit"
                            sx={{ mt: 3, mb: 2 }}
                            variant="contained"
                            fullWidth
                            onClick={handleRegister.bind(this)}>Register</Button>
                        <Grid container>
                            <Grid item xs>
                                <Link
                                    variant="body2"
                                    onClick={handleNewUser.bind(this)}>Already have an account? Login</Link>
                            </Grid>
                        </Grid>
                    </Box>
                }
                {useSelector(authorized) &&
                    <Redirect to="/home" />
                }
                {useSelector(hasError) &&
                    <strong>{errorMessage.message || errorMessage}</strong>
                }
            </Box>
        </Box>
    );
};
