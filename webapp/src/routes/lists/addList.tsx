import React, { useEffect, useState } from "react";
import Overlay from "../../components/overlay/overlay";
import Grid from "../../components/grid/grid"
import { useSelector, useDispatch } from "react-redux";
import { words } from "../../slices/wordsSlice";
import { addingList, addList, editingList, editList } from "../../slices/listsSlice";
import { getWords } from "../../slices/wordsSlice";

import ListData from "../../Types/ListData";
import WordData from "../../Types/WordData";

type AddListProps = { listData?: ListData | null, editing?: boolean };

export default function AddList(props:AddListProps) {

    const dispatch = useDispatch();

    const data = useSelector(words);
    const [newListName, setNewListName] = useState<string>(props.listData ? props.listData.name : '');
    const [selectedRows, setSelectedRows] = useState<WordData[]>(props.listData ? props.listData.words : []);
    const columns = [
        {
            header: "Word",
            key: "word",
            formatter: null
        },
        {
            header: "Reading",
            key: "wordReading",
            formatter: null
        },
        {
            header: "Translation",
            key: "wordTranslation",
            formatter: null
        },
    ];

    useEffect(() => {
        dispatch(getWords());
    },[dispatch]);

    const handleClose = () => {
        if (props.editing) {
            dispatch(editingList(false));
            return;
        }
        dispatch(addingList(false));
    };

    const handleSubmit = () => {
        if (props.editing && props.listData) {
            dispatch(editList({
                _id: props.listData._id,
                name: newListName,
                words: selectedRows
            }));
            return;
        }
        dispatch(addList({
            name: newListName,
            words: selectedRows
        }));
    };

    const handleRowSelected = (selectedRows: []) => {
        setSelectedRows(selectedRows);
    };

    return (
        <Overlay title={props.editing ? `Edit ${props.listData && props.listData.name}` : "Add List"}
            onClose={handleClose}
            buttons={{
                right: [
                    { label: 'Close', classNames: 'close', onClick: handleClose },
                    { label: 'Submit', classNames: 'submit', onClick: handleSubmit }
                ]
            }}>
            <form
                onSubmit={evt => evt.preventDefault()}>
                <label className="flex-col">
                    List Name <input
                        onChange={(evt) => setNewListName(evt.target.value)}
                        type="text"
                        name="name"
                        defaultValue={props.editing && props.listData ? props.listData.name : ''} />
                </label>
            </form>
            <div className="flex-col">
                <Grid
                    columns={columns}
                    data={data}
                    useId={'_id'}
                    rowSelector={true}
                    selectedRows={selectedRows.length ?
                        selectedRows.reduce((
                            agg: {
                                [key: string]: WordData
                            },
                            word: WordData) => {
                                agg[word._id] = word;
                                return agg;
                            }, {}): null}
                    onRowSelected={handleRowSelected} />
            </div>
        </Overlay>
    );
};