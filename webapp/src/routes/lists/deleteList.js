import Overlay from "../../components/overlay/overlay";

export default function DeleteList(props) {
    return (
        <Overlay title="Delete List"
            onClose={props.onClose}
            buttons={{
                right: [
                    { label: 'Close', classNames: 'close', onClick: props.onClose },
                    {
                        label: 'Delete', classNames: 'delete', onClick: props.onDelete
                    }
                ]
            }}>
            <p>Are you sure you want to delete the selected list?</p>
        </Overlay>
    );
}