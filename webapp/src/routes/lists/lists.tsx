import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/grid/grid";
import CardViewer from "../../components/cardViewer/cardViewer";
import AddList from "./addList";
import DeleteList from "./deleteList";
import {
    editing,
    editingList,
    adding,
    addingList,
    deleting,
    deletingList,
    deleteList,
    getLists,
    lists
} from "../../slices/listsSlice";

import ListData from "../../Types/ListData";

export default function Lists() {

    const columns = [
        {
            header: 'Name',
            key: 'name'
        },
        {
            header: 'Total Words',
            key: 'totalWords',
            formatter: (rowData: ListData) => {
                return String(rowData.words.length);
            },
            width: 50
        },
        {
            header: 'Review Words',
            key: 'review',
            formatter: (rowData: ListData) => {
                return <button onClick={() => {viewWords(rowData)}}>View</button>;
            },
            width: 50
        },
        {
            header: 'Edit List',
            key: 'edit',
            formatter: (rowData: ListData) => {
                return <button onClick={() => {editList(rowData)}}>Edit</button>;
            },
            width: 50
        },
    ];
    const [selectedRow, setSelectedRow] = useState<ListData | null>(null);
    const [viewingWords, setViewingWords] = useState(false);
    const isEditing = useSelector(editing);
    const isDeleting = useSelector(deleting);
    const isAdding = useSelector(adding);
    const dispatch = useDispatch();
    const data = useSelector(lists);

    useEffect(() => {
        dispatch(getLists());
    }, [dispatch])

    const viewWords = (listData: ListData) => {
        setSelectedRow(listData);
        setViewingWords(!viewingWords);
    };

    const editList = (listData: ListData) => {
        setSelectedRow(listData);
        dispatch(editingList(true));
    }

    const handleDelete = (rowData: ListData) => {
        dispatch(deleteList(rowData._id));
    }

    const renderAddList = () => {
        return isAdding ? <AddList /> : null;

    }

    const renderEditList = () => {
        if (isEditing && selectedRow) {
            return <AddList editing={true} listData={selectedRow} />
        }
        return null;
    }

    const renderDeleteList = () => {
        if (isDeleting) {
            return (
            <DeleteList
                onClose={() => { dispatch(deletingList(false)) }}
                onDelete={() => {
                    if (selectedRow) {
                        handleDelete(selectedRow);
                    }
                }}
            />);
        }
        return null;

    }

    const renderCardViewer = () => {
        if (viewingWords &&
            selectedRow) {
            return <CardViewer title={selectedRow.name}
                data={selectedRow && selectedRow.words}
                frontKey="word"
                onClose={() => { setViewingWords(false) }} />
        }
        return null;
    }

    return (
        <div className="route-content">
            <h2 className="route-header">Lists</h2>
            <div className="route-body">
                <div className="route-controls">
                    <div className="region"></div>
                    <div className="region">
                        <button className="add" onClick={() => { dispatch(addingList(true)) }}>Add List</button>
                    </div>
                </div>
                <div className="route-view">
                    <Grid
                        columns={columns}
                        data={data}
                        useId={'_id'}
                        deletable={true}
                        onDelete={(rowData: ListData) => {
                            setSelectedRow(rowData);
                            dispatch(deletingList(true));
                        }} />
                </div>
            </div>
            {renderAddList()}
            {renderEditList()}
            {renderDeleteList()}
            {renderCardViewer()}
        </div>
    );
};