import React, {useState} from 'react';
import { useSelector, useDispatch } from "react-redux";

import {Link} from 'react-router-dom';

import {
    authorized,
    logout
} from '../slices/authSlice';

import {
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    Drawer,
    List,
    ListItem,
    Link as MUILink
} from "@mui/material";

import Menu from '@mui/icons-material/Menu';

export default function TheHeader() {

    const isAuthorized = useSelector(authorized);
    const [menuState, setMenuState] = useState({ open: false });
    const dispatch = useDispatch();

    const toggleMenu = () => {
        setMenuState({open: !menuState.open});
    }

    return (
        < AppBar position="static" >
            <Toolbar>
                <IconButton className='hamburger'
                    onClick={toggleMenu} title="Menu" color="inherit">
                    <Menu />
                </IconButton>
                <Typography>JP Flash</Typography>
            </Toolbar>
            {
                isAuthorized &&
                <Drawer
                    open={menuState.open}
                    onClose={toggleMenu}>
                    <List onClick={toggleMenu}>
                        <ListItem><Link to="/home">Home</Link></ListItem>
                        <ListItem><Link to="/quiz">Quiz</Link></ListItem>
                        <ListItem><Link to="/lists">Lists</Link></ListItem>
                        <ListItem><Link to="/words">Words</Link></ListItem>
                        <ListItem><Link to="/resListts">Results</Link></ListItem>
                        <ListItem><MUILink onClick={() => { dispatch(logout()); }}>Logout</MUILink></ListItem>
                    </List>
                </Drawer>
            }
        </AppBar >
    );
}
