import WordData, { FrontKey } from "../../Types/WordData";
import Card from "../card/card";
import Overlay from "../overlay/overlay";

type CardViewerProps = {
    title: string,
    data: WordData[],
    frontKey: FrontKey,
    onClose: Function
}

const defaultProps = (): CardViewerProps =>{
    return {
        title: 'Title',
        data: [],
        frontKey: "word",
        onClose: () => { }
    };
};

export default function CardViewer(props: CardViewerProps = defaultProps()) {
    return (
        <Overlay
            title={props.title}
            onClose={props.onClose}
            buttons={{
                left: [],
                right: [
                    {
                        label: 'Close',
                        classNames: 'close',
                        onClick: props.onClose
                    }
                ]
            }}>
            <div className="card-viewer flex-row flex-wrap flex-main flex-center">
                {props.data.map((item: WordData) => {
                    return (
                        <Card
                            data={item}
                            frontKey={props.frontKey} />
                    );
                })}
            </div>
        </Overlay>
    );
}