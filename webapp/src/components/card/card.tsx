import React, { useEffect, useState } from "react";
import WordData, {FrontKey} from "../../Types/WordData";

type CardField = {
    header: string,
    key: FrontKey,
    formatter?: Function
}

export const fields: CardField[] = [
    {
        header: "Word",
        key: "word",
    },
    {
        header: "Reading",
        key: "wordReading"
    },
    {
        header: "Translation",
        key: "wordTranslation"
    },
    {
        header: "Sentence",
        key: "sentence"
    },
    {
        header: "Reading",
        key: "sentenceReading"
    },
    {
        header: "Translation",
        key: "sentenceTranslation"
    },
];

type DefaultCardProps = {
    classNames?: string,
    data: WordData,
    frontKey: FrontKey,
    flipped?: boolean,
    flipOnce?: boolean,
    onFlipped?: Function
}

export default function Card(
    props: DefaultCardProps = {
        classNames: '',
        data: {} as WordData,
        frontKey: 'word',
        flipped: false,
        flipOnce: false,
        onFlipped: () => {}
    }) {

    const [flipped, setFlipped] = useState(props.flipped);

    const handleFlipped = () => {
        if (props.flipOnce && flipped) {
            return;
        }
        setFlipped(!flipped);
        if (props.onFlipped) {
            props.onFlipped(flipped);
        }
    };

    useEffect(() => {
        setFlipped(props.flipped);
    }, [props, setFlipped]);


    return (
        <div className={`card flex-row ${flipped ? 'flipped' : ''} ${props.classNames}`}
            onClick={handleFlipped}>
            <div className="card-content">
                <div className="card-face card-front flex-col flex-main">
                    <div className="card-face-content flex-main flex-col">
                        <div className="flex-row">
                            {props.data[props.frontKey]}
                        </div>
                    </div>
                </div>
                <div className="card-face card-back flex-col flex-main">
                    <div className="card-face-content flex-main table">
                        {fields.map(field => {
                            return (
                                <div key={field.key} className="card-field table-row">
                                    <span className="field-label table-cell">{field.header}</span>
                                    <span className="field-value table-cell">{props.data[field.key]}</span>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};