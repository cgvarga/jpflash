import React from "react";

export default class Pagination extends React.Component {
    state = {
        currentPage: this.props.page,
        pageSize: 25,
        totalPages: this.props.page,
        firstEnabled: false,
        prevEnabled: false,
        nextEnabled: false,
        lastEnabled: false,
    }

    _handlePageInput(evt) {
        this.setState({ currentPage: 0 });
        this._handlePageChange(this.state.currentPage);
    }

    _handlePaginationButtonClick(action) {
        const actionMap = {
            first: () =>{
                this.setState({ currentPage: 0 }, () => {
                    this._handlePageChange(this.state.currentPage);
                    this._checkButtonStates();
                });
            },
            prev: () => {
                if (this.state.currentPage === 0) {
                    return;
                }
                this.setState({ currentPage: this.state.currentPage - 1 }, () => {
                    this._handlePageChange(this.state.currentPage);
                    this._checkButtonStates();
                });
            },
            next: () => {
                if (this.state.currentPage === this.state.totalPages - 1) {
                    return;
                }

                this.setState({ currentPage: this.state.currentPage + 1 }, () => {
                    this._handlePageChange(this.state.currentPage);
                    this._checkButtonStates();
                });
            },
            last: () => {
                this.setState({ currentPage: this.state.totalPages - 1 }, () => {
                    this._handlePageChange(this.state.currentPage);
                    this._checkButtonStates();
                });
            },
        }

        if (actionMap[action]) {
            actionMap[action]();
        }
    }

    _handlePageChange(currentPage) {
        if (this.props.onPageChange) {
            this.props.onPageChange(currentPage);
        }
    }

    _handlePageSizeChange(evt) {
        const size = parseInt(evt.target.value, 10);
        this.setState({ pageSize: size, currentPage: 0 }, () => {
            this.props.onPageSizeChange(size);
            this._checkButtonStates();
        });
    }

    componentDidMount() {
        this._checkButtonStates();
    }

    _checkButtonStates() {
        const totalPages = Math.floor(this.props.data.length / this.state.pageSize) + 1;
        const newState = Object.assign({}, this.state);

        newState.totalPages = totalPages;

        if (this.state.currentPage > 0) {
            newState.firstEnabled = true;
            newState.prevEnabled = true;
        } else {
            newState.firstEnabled = false;
            newState.prevEnabled = false;
        }
        if (this.state.currentPage < totalPages - 1) {
            newState.nextEnabled = true;
            newState.lastEnabled = true;
        } else {
            newState.nextEnabled = false;
            newState.lastEnabled = false;
        }

        this.setState(newState);
    }

    render() {
        return (
            <div className="pagination">
                <select className="page-size" defaultValue={this.state.pageSize}
                    title="Page Size"
                    onChange={this._handlePageSizeChange.bind(this)}>
                    {this.props.pageSizes.map((size) => {
                        return <option value={size} key={`page-size-${size}`}>{size}</option>
                    })}
                </select>
                <button className="pagination-button first" title="First Page"
                    onClick={this._handlePaginationButtonClick.bind(this, 'first')}
                    disabled={!this.state.firstEnabled}>&lt;&lt;</button>
                <button className="pagination-button prev" title="Previous Page"
                    onClick={this._handlePaginationButtonClick.bind(this, 'prev')}
                    disabled={!this.state.prevEnabled}>&lt;</button>
                <div className="pagination-status">
                    <input type="number" className="current-page" title="Current Page"
                        onChange={this._handlePageInput.bind(this, 'prev')}
                        value={this.state.currentPage + 1}
                        max={this.state.totalPages}/>
                        of {this.state.totalPages}
                </div>
                <button className="pagination-button next" title="Next Page"
                    onClick={this._handlePaginationButtonClick.bind(this, 'next')}
                    disabled={!this.state.nextEnabled}>&gt;</button>
                <button className="pagination-button last" title="Last Page"
                    onClick={this._handlePaginationButtonClick.bind(this, 'last')}
                    disabled={!this.state.lastEnabled}>&gt;&gt;</button>
            </div>
        );
    }

};

Pagination.defaultProps = {
    data: [],
    page: 0,
    pageSizes: [5, 10, 25, 50, 100],
    onPageChange: () => {},
    onPageSizeChange: () => {}
}