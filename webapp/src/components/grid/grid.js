import React from "react";
import Pagination from "./pagination";

export default class Grid extends React.Component {

    constructor(props) {
        super(props);
        let columns = this.props.columns.slice();
        this.state = {
            filter: '',
            currentPage: 0,
            pageSize: 25,
            data: this.props.data,
            columns: columns,
            viewData: this.props.data.slice() || [],
            cards: false,
            selectAllChecked: false,
            selectedRows: props.selectedRows || {}
        }
    }
    componentDidMount() {
        const newState = Object.assign({}, this.state);
        newState.viewData = this._filterData();

        if (this.props.rowSelector) {
            newState.columns = [
                {
                    header: <input
                        type="checkbox"
                        defaultChecked={this.state.selectAllChecked}
                        onChange={this._handleSelectAll.bind(this)} />,
                    key: "selector",
                    formatter: (rowData) => {
                        return <input
                            type="checkbox"
                            checked={this.state.selectedRows[rowData[this.props.useId]] !== undefined}
                            onChange={this._handleRowCheckboxChange.bind(this, rowData)} />
                    },
                    width: 25
                }].concat(newState.columns);
        }
        if (this.props.deletable) {
            newState.columns.push({
                header: 'Delete',
                key: "delete",
                formatter: (rowData) => {
                    return <button className="delete"
                        onClick={() => this.props.onDelete(rowData)}>Delete</button>
                },
                width: 50
            });
        }
        this.setState(newState);
    }
    componentDidUpdate() {
        if (JSON.stringify(this.props.data) !== JSON.stringify(this.state.data)) {
            const newState = Object.assign({}, this.state);
            newState.data = this.props.data;
            newState.viewData = this._filterData();
            this.setState(newState);
        }

    }
    _handleFilterInput(evt) {
        this.setState({
            filter: evt.target.value,
            viewData: this._filterData(evt.target.value)
        });
    }
    _filterData(filter = '') {
        let data = Object.assign([], this.props.data);
        let filtered = data.filter((row) => {
            return Object.keys(row).some(key => {
                return String(row[key]).includes(filter);
            });
        });
        return filtered;
    }
    _handleSelectAll(evt) {
        const checked = evt.target.checked;
        const newState = Object.assign({}, this.state);
        newState.selectedRows = {};
        newState.selectAllChecked = false;
        if (checked) {
            //selectall
            newState.selectAllChecked = true;
            this._getPageData().forEach((rowData) => {
                newState.selectedRows[rowData[this.props.useId]] = rowData;
            });
        }

        this.setState(newState, () => {
            this.props.onRowSelected(Object.keys(this.state.selectedRows).map(key => {
                return this.state.selectedRows[key];
            }));
        });
    }
    _handleRowCheckboxChange(rowData, evt) {
        const newState = Object.assign({}, this.state);
        if (newState.selectedRows[rowData[this.props.useId]]) {
            delete newState.selectedRows[rowData[this.props.useId]];
        } else {
            newState.selectedRows[rowData[this.props.useId]] = rowData;
        }
        newState.selectAllChecked = Object.keys(newState.selectedRows).length === this.state.viewData.length;
        this.setState(newState, () => {
            this.props.onRowSelected(Object.keys(this.state.selectedRows).map(key => {
                return this.state.selectedRows[key];
            }));
        });
    }
    _handlePageChange(currentPage) {
        this.setState(Object.assign({}, this.state, {
            currentPage,
            viewData: this._filterData(),
            selectAllChecked: false,
            selectedRows: this.props.crossPageSelections ?
                Object.assign({}, this.state.selectedRows) : {}
        }));
    }
    _handlePageSizeChange(pageSize) {
        this.setState(Object.assign ({}, this.state, {
            pageSize,
            viewData: this._filterData(),
            selectAllChecked: false,
            selectedRows: this.props.crossPageSelections ?
                Object.assign({}, this.state.selectedRows) : {}
        }));
    }
    _handleToggleCards() {
        this.setState({cards: !this.state.cards});
    }
    _getPageData() {
        const firstRecord = Math.max(0, this.state.currentPage * this.state.pageSize - 1);
        return this.state.viewData.slice(firstRecord, firstRecord + this.state.pageSize);
    }
    render() {
        return (
            <div className={`grid ${this.state.cards && 'cards'}`}>
                <div className="grid-controls">
                    {this.props.filterBar &&
                    <div className="grid-filter">
                        <input placeholder="Filter" type="text" name="filter"
                            defaultValue={this.state.filter}
                            onInput={this._handleFilterInput.bind(this)}/>
                    </div>
                    }
                    {this.props.pagination &&
                        <Pagination
                            data={this.state.viewData}
                            page={this.state.currentPage}
                            onPageChange={this._handlePageChange.bind(this)}
                            onPageSizeChange={this._handlePageSizeChange.bind(this)}/>
                    }
                    <div>
                        <button className="cards-toggle"
                            title="Toggle Card View"
                            onClick={this._handleToggleCards.bind(this)}>[]</button>
                    </div>
                </div>
                <div className="grid-content">
                    <table>
                        <thead>
                            <tr>
                                {this.state.columns.map(col => {
                                    return <th key={`grid-${col.key}`} style={{width: (col.width && col.width + 'px') || 'auto'}}>{col.header}</th>
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {this._getPageData().map((rowData, i) => {
                                return <tr key={`grid-row-${i}`}
                                    tabIndex="-1"
                                    onClick={() => this.props.onRowClick(rowData)}>
                                    {this.state.columns.map((col) => {
                                        return <td key={`grid-row-${i}-${col.key}`}>
                                            <div className="column-label">{col.header}</div>
                                            <div className="cell-value">{(col.formatter && col.formatter(rowData)) || rowData[col.key]}</div>
                                        </td>
                                    })}
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Grid.defaultProps = {
    columns: [
        {
            header: "Column 1",
            key: "a",
            formatter: null
        },
        {
            header: "Column 2",
            key: "b",
            formatter: (rowData) => {
                return new Date(rowData.b).toDateString();
            }
        },
    ],
    data: [
        {id: 1, a: "Row 1", b: new Date('2021-08-01').getTime()},
        {id: 2, a: "Row 2", b: new Date().getTime()},
    ],
    filterBar: true,
    pagination: true,
    onRowClick: (rowData) => {},
    rowSelector: false,
    crossPageSelections: true,
    selectedRows: null,
    onRowSelected: (selectedRows) => {},
    useId: 'id',
    deletable: false,
    onDelete: () => {}
};