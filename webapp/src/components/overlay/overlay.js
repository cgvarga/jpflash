import { Component } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, IconButton } from '@mui/material'
import Close from "@mui/icons-material/Close";

export default class Overlay extends Component {

    render() {
        return (
            <Dialog
                open={true}
            >
                <DialogTitle>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center">
                        <Grid item>
                            {this.props.title}
                        </Grid>
                        <Grid item>
                            <IconButton
                                onClick={this.props.onClose}><Close />
                            </IconButton>
                        </Grid>
                    </Grid>
                </DialogTitle>
                <DialogContent>
                    {this.props.children}
                </DialogContent>
                <DialogActions>
                    {this.props.buttons && this.props.buttons.left &&
                        <div className="dialog-footer-region flex-main flex-row flex-space-between">
                            {this.props.buttons.left.map((button) => {
                                return <Button className={button.classNames} key={`footer-left-${button.label}`}
                                    onClick={button.onClick}>{button.label}</Button>
                            })}
                        </div>
                    }
                    {this.props.buttons && this.props.buttons.right &&
                        <div className="dialog-footer-region flex-main flex-row flex-space-between">
                            {this.props.buttons.right.map((button) => {
                                return <Button className={button.classNames} key={`footer-right-${button.label}`}
                                    onClick={button.onClick}>{button.label}</Button>
                            })}
                        </div>
                    }
                    <Button
                        onClick={this.props.onClose}
                    >Close</Button>
                </DialogActions>
            </Dialog>
        );
    };

    // render() {
    //     return (
    //         <div className="overlay">
    //             <div className="dialog" tabIndex="-1" role="dialog" aria-label={this.props.title}>
    //                 <div className="header dialog-header">
    //                     <div className="dialog-title">{this.props.title}</div>
    //                     <button className="close" onClick={this.props.onClose}>Close</button>
    //                 </div>
    //                 <div className="dialog-content content flex-col flex-main">
    //                     {this.props.children}
    //                 </div>
    //                 <div className="dialog-footer footer flex-row flex-space-between">
    //                     {this.props.buttons && this.props.buttons.left &&
    //                         <div className="dialog-footer-region flex-main flex-row flex-space-between">
    //                         {this.props.buttons.left.map((button) => {
    //                             return <button className={button.classNames} key={`footer-left-${button.label}`}
    //                                 onClick={button.onClick}>{button.label}</button>
    //                         })}
    //                         </div>
    //                     }
    //                     {this.props.buttons && this.props.buttons.right &&
    //                         <div className="dialog-footer-region flex-main flex-row flex-space-between">
    //                             {this.props.buttons.right.map((button) => {
    //                                 return <button className={button.classNames} key={`footer-right-${button.label}`}
    //                                     onClick={button.onClick}>{button.label}</button>
    //                             })}
    //                         </div>
    //                     }
    //                 </div>
    //             </div>
    //         </div>
    // );
    // }
};

Overlay.defaultProps = {
    onClose: () => { },
    buttons: {
        left: [],
        right: [
            {
                label: 'Close',
                classNames: 'close'
            }
        ]
    }
};