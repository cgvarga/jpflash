import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../slices/authSlice';
import listsReducer from '../slices/listsSlice';
import wordsReducer from '../slices/wordsSlice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    words: wordsReducer,
    lists: listsReducer
  },
});
