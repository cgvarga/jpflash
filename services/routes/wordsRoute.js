import express from "express";
import {
    getWords,
    getWord,
    createWord,
    deleteWord
} from "../controllers/wordsController.js";


const router = express.Router();

router.get('/', getWords);
router.post('/', createWord);

router.get('/:id', getWord);
router.delete('/:id', deleteWord);

export default router;