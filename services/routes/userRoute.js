import { Router } from "express";
import { register, login, logout, getUser } from '../controllers/userController.js';
import passport from 'passport';

const router = new Router();

router.post('/login', passport.authenticate('local'), login);
router.post('/register', register);
router.post('/logout', logout);
router.get('/', getUser);

export default router;