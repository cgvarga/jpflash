import { Router } from 'express';
import passport from 'passport';
import {getLists, createList, updateList, deleteList} from '../controllers/listsController.js';

const router = new Router();

router.get('/',  getLists);
router.post('/',  createList);
router.put('/:id',  updateList);
router.delete('/:id',  deleteList);


export default router