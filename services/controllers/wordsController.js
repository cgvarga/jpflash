import WordModel from "../models/wordModel.js";
import standardResponse from "../models/standardResponse.js";

export const getWords = async (req, resp) => {
    const words = await WordModel.find();
    try {
        resp.status(200).json(words);
    } catch (error) {
        resp.status(500).json(standardResponse(false, error));
    }
};

export const createWord = async (req, resp) => {
    const word = req.body;
    const newWord = new WordModel(word);
    try {
        await newWord.save();
        resp.status(200).json(standardResponse(true, `successfully saved ${word.word}`));
    } catch (error) {
        resp.status(500).json(standardsResponse(false, error));
    }
};

export const getWord = async (req, resp) => {
    resp.json(standardResponse(true, `getWord ${req.params.id}`));
};

export const deleteWord = async (req, resp) => {
    resp.json(standardResponse(true, `deleteWord ${req.params.id}`));
};

export const updateWord = async (req, resp) => {
    resp.json(standardResponse(true, `updateWord ${req.params.id}`));
};