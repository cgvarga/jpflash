import ListModel from "../models/listModel.js";
import standardResponse from "../models/standardResponse.js";


export const getLists = async (req, resp) => {
    const lists = await ListModel.find({owner: req.user});
    if (lists) {
        resp.status(200).json(lists);
        return;
    }

    resp.status(200).json([]);

};

export const createList = async (req, resp) => {
    const newList = new ListModel({
        ...req.body,
        owner: req.user
    });

    try {
        await newList.save();
        resp.status(200).json(standardResponse(true, `Successfully created ${req.body.name}`));
    } catch (error) {
        console.log(error);
        resp.status(500).json(standardResponse(false, error));
    }
};

export const updateList = async (req, resp) => {
    const {name, words} = req.body;
    const list = await ListModel.findOne({_id: req.params.id, owner: req.user});
    if (list) {
        list.name = name ? name : list.name;
        list.words = words !== undefined ? words : list.words;
        try {
            list.save();
            resp.status(200).json(standardResponse(true, `Successfully updated ${req.body.name}`));
        } catch (error) {
            resp.status(500).json(standardResponse(false, error));
        }
    } else {
        resp.status(500).json(standardResponse(false, `List with id ${req.params.id} not found.`));
    }
};

export const deleteList = async (req, resp) => {
    const list = await ListModel.find({ _id: req.params.id, owner: req.user });
    const name = list.name;
    if (list) {
        try {
            await ListModel.deleteOne({ _id: req.params.id, owner: req.user });
            resp.status(200).json(standardResponse(true, `Successfully deleted ${name}`));
        } catch (error) {
            throw error;
        }
    } else {
        resp.status(500).json(standardResponse(false, `List with id ${req.params.id} not found.`));
    }
}