import UserModel from '../models/userModel.js';
import bcrypt from 'bcryptjs';
import standardResponse from '../models/standardResponse.js';

export const register = async (req, resp, next) => {
    const user = await UserModel.findOne({username: req.body.username});
    if (user) {
        resp.status(500).json(standardResponse(false, 'User already Exists'));
        return;
    }
    try {
        const hashPW = await bcrypt.hash(req.body.password, 10);
        const newUser = new UserModel({
            username: req.body.username,
            password: hashPW,
            email: req.body.email
        });
        await newUser.save();
        resp.status(200).json(standardResponse(true, `successfully registered ${req.body.username}`));
    } catch (error) {
        resp.status(500).json(error);
    }
};

export const login = (req, resp) => {
    resp.json(standardResponse(true, 'Successfully Authenticated'));
};

export const logout = (req, resp) => {

};

export const getUser = (req, resp) => {
    const user = req.user;
    if(user) {
        resp.status(200).json(user);
    } else {
        resp.status(500).json(standardResponse(false, "Invalid User"));
    }
};