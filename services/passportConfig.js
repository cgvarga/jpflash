import UserModel from "./models/userModel.js";
import PassportLocal from "passport-local";
import bcrypt from 'bcryptjs';

const localStrategy = PassportLocal.Strategy;

export default function (passport) {
    passport.use(
        new localStrategy({
            usernameField: 'username'
        },(username, password, done) => {
            UserModel.findOne({username}, (err, user) => {
                if(err) throw error;
                if(!user) return done(null, false);
                bcrypt.compare(password, user.password, (err, result) => {
                    if (err) throw err;
                    if (result === true) {
                        return done(null, user);
                    } else {
                        return done(null, false);
                    }
                });
            });
        })
    );

    passport.serializeUser((user, cb) => {
        cb(null, user._id);
    });
    passport.deserializeUser((id, cb) => {
        console.log('\n deserializeUser', id);
        UserModel.findOne({ _id: id }, (err, user) => {
            console.log('user', user);
            cb(err, user);
        });
    });
};

export const authenticationMiddleware = (whiteList = []) => (req, res, next) => {
    console.log('\n authenticationMiddleware')
    if (whiteList.includes(req.url)) {
        return next();
    }

    console.log('req.isAuthenticated()', req.isAuthenticated());

    if (req.isAuthenticated()) {
        return next();
    }

    throw "invalid user"
};