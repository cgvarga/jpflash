import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import userRoute from "./routes/userRoute.js";
import wordsRoute from "./routes/wordsRoute.js";
import listsRoute from "./routes/listsRoute.js";
import dotenv from "dotenv";
import session from 'express-session';
import cookieParser from "cookie-parser";
import passport from "passport";
import passportConfig, {authenticationMiddleware} from "./passportConfig.js";

const app = express();
dotenv.config();
const PORT = process.env.PORT || "5000";
const WHITELIST = process.env.WHITELIST.split(',');
const ORIGINS = process.env.ORIGIN && process.env.ORIGIN.split(',') || true;
console.log(ORIGINS);

app.use(cors({
    origin: true,
    // origin: process.env.ORIGIN,
    credentials: true
}));
app.use(express.json({}));
app.use(express.urlencoded({}));

app.use(cookieParser(process.env.SECRET))

const cookieConfig = {};

if (process.env.LIVE === 'true') {
    cookieConfig.path = process.env.COOKIE_PATH;
    cookieConfig.sameSite = process.env.SAMESITE || false;
    cookieConfig.secure = process.env.SECURE === 'true';
}

//for heroku
//https://stackoverflow.com/questions/11277779/passportjs-deserializeuser-never-called#answer-63532477
app.set('trust proxy', 1);

app.use(session({
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: cookieConfig
}));;

app.use(passport.initialize());
app.use(passport.session());
passportConfig(passport);

app.use(authenticationMiddleware(WHITELIST));

mongoose.connect(process.env.CONNECTION_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        app.listen(PORT, () => {
            console.log(`jpflash server is running on ${PORT} and connected to DB.`);
        });
    });

app.get('/', (req, resp) => {
    resp.send('JPFLASH SERVER ROOT');
});

app.use('/user', userRoute);

app.use('/words', wordsRoute);

app.use('/lists', listsRoute);
