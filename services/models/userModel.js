import mongoose from "mongoose";

export const schema = new mongoose.Schema({
    username: String,
    password: String,
    email: String
});

const model = new mongoose.model('user', schema);

export default model;