const standardResponse = (isSuccess = true, message) => {
    return {
        status: isSuccess ? 'success' : 'error',
        message: message.stack ? message.stack : message
    }
};

export default standardResponse;

