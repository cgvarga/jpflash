import mongoose from 'mongoose';
import {schema as WordSchema} from './wordModel.js';
import {schema as UserSchema} from './userModel.js';

export const schema = mongoose.Schema({
    name: String,
    words: [WordSchema],
    owner: UserSchema
});

export default mongoose.model('List', schema);