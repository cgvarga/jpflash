import mongoose from "mongoose";

export const schema = mongoose.Schema({
    word: String,
    wordReading: String,
    wordTranslation: String,
    sentence: String,
    sentenceReading: String,
    sentenceTranslation: String,
    rank: Number
});

export default mongoose.model('Word', schema);